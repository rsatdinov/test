<?php

namespace Task\Discount\Rules;

use Task\IOrderProcessor;

class Multi implements IRule
{
    private $required_item;
    private $sub_items;
    private $discount;

    public function __construct($rule)
    {
        $this->required_item = $rule['required_item'];
        $this->sub_items = $rule['sub_items'];
        $this->discount = $rule['discount'];
    }

    public function setDiscount(IOrderProcessor $order)
    {
        $discount_product_position = [
            $this->required_item => [],
            'sub_items' => []
        ];

        foreach ($order->getList() as $key => $product) {
            $product_name = $product->getName();
            if (null === $product->getDiscount()) {
                if ($product_name === $this->required_item) {
                    $discount_product_position[$product_name][] = $key;
                } else if (in_array($product_name, $this->sub_items, true)) {
                    $discount_product_position['sub_items'][] = $key;
                }
            }
        }

        foreach ($discount_product_position[$this->required_item] as $key => $required_item_pos) {
            if (isset($discount_product_position['sub_items'][$key])) {
                $product = $order->getList()[$required_item_pos];
                $product->setDiscount($this->discount);

                $sub_item_pos = $discount_product_position['sub_items'][$key];
                $product = $order->getList()[$sub_item_pos];
                $product->setDiscount($this->discount);
            }
        }
    }
}