<?php

namespace Task\Discount\Rules;

use Task\IOrderProcessor;

class Triple implements IRule
{
    private $first_item;
    private $second_item;
    private $third_item;
    private $discount;

    public function __construct($rule)
    {
        $this->first_item = $rule['first_item'];
        $this->second_item = $rule['second_item'];
        $this->third_item = $rule['third_item'];
        $this->discount = $rule['discount'];
    }

    public function setDiscount(IOrderProcessor $order)
    {
        $counter = [
            $this->first_item => 0,
            $this->second_item => 0,
            $this->third_item => 0,
        ];

        $discount_product_position = [
            $this->first_item => [],
            $this->second_item => [],
            $this->third_item => [],
        ];

        foreach ($order->getList() as $key => $product) {
            $product_name = $product->getName();
            if (
                ($product_name === $this->first_item ||
                    $product_name === $this->second_item ||
                    $product_name === $this->third_item) &&
                null === $product->getDiscount()
            ) {
                $counter[$product_name]++;
                $discount_product_position[$product_name][] = $key;
            }
        }

        $count_discount = min($counter);
        for ($i = 0; $i < $count_discount; $i++) {
            $positions = [
                $discount_product_position[$this->first_item][$i],
                $discount_product_position[$this->second_item][$i],
                $discount_product_position[$this->third_item][$i]
            ];

            foreach ($positions as $position) {
                $product = $order->getList()[$position];
                $product->setDiscount($this->discount);
            }
        }
    }
}