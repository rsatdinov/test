<?php

namespace Task\Discount\Rules;

use Task\IOrderProcessor;

interface IRule
{
    public function setDiscount(IOrderProcessor $order);
}