<?php

namespace Task\Discount;

use Task\Discount\Rules\Double;
use Task\Discount\Rules\Multi;
use Task\Discount\Rules\Common;
use Task\Discount\Rules\Triple;
use Task\IOrderProcessor;

class Discount
{
    private $rules_collection;

    public function __construct()
    {
        $rules_array = json_decode(file_get_contents('rules.json'), true);
        foreach ($rules_array as $rule) {
            switch ($rule['type']) {
                case 'double':
                    $this->rules_collection[] = new Double($rule);
                    break;
                case 'triple':
                    $this->rules_collection[] = new Triple($rule);
                    break;
                case 'multi':
                    $this->rules_collection[] = new Multi($rule);
                    break;
                case 'common':
                    $this->rules_collection[] = new Common($rule);
                    break;
                default:
                    die('Invalid rule');
            }
        }
    }

    private function getRules()
    {
        return $this->rules_collection;
    }

    public function initDiscount(IOrderProcessor $order)
    {
        foreach ($this->getRules() as $rule) {
            $rule->setDiscount($order);
        }
    }
}
