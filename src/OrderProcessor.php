<?php

namespace Task;

use Task\Discount\Discount;
use Task\Product\IProduct;
use Task\Product\Product;

class OrderProcessor implements IOrderProcessor
{
    private $catalog;
    private $order = [];
    private $common_discount;
    private $total_sum = 0;

    public function __construct($order)
    {
        $this->setCatalog();
        $this->init($order);

        $discount = new Discount();
        $discount->initDiscount($this);

        $this->calculateTotalSum();
    }

    private function init($list)
    {
        foreach ($list as $key => $item) {
            $this->validateItem($item);

            $product = [
                'name' => $item,
                'price' => $this->catalog[$item],
            ];

            $this->addItem(new Product($product));
        }
    }

    private function validateItem($item)
    {
        if (!isset($this->catalog[$item])) {
            die('invalid product');
        }
    }

    private function setCatalog()
    {
        $this->catalog = json_decode(file_get_contents('catalog.json'), true);
    }

    public function getList()
    {
        return $this->order;
    }

    public function addItem(IProduct $item)
    {
        $this->order[] = $item;
    }

    public function deleteItem($id)
    {
        unset($this->order[$id]);
    }

    public function setCommonDiscount($discount)
    {
        $this->common_discount = $discount;
    }

    public function getCommonDiscount()
    {
        return $this->common_discount;
    }

    public function calculateTotalSum()
    {
        $this->total_sum = 0;
        foreach ($this->getList() as $product) {
            if (null !== $product->getDiscount()) {
                $discount = $product->getPrice() * $product->getDiscount() / 100;
                $this->total_sum += round($product->getPrice() - $discount, 2);
            }
        }

        if (null !== $this->getCommonDiscount()) {
            $discount = $this->total_sum * $this->getCommonDiscount() / 100;
            $this->total_sum = round($this->total_sum - $discount, 2);
        }
    }

    public function getTotalSum()
    {
        return $this->total_sum;
    }
}
