<?php

namespace Task\Product;


class Product implements IItem, IProduct
{
    private $name;
    private $price;
    private $discount;

    public function __construct($product)
    {
        $this->setName($product['name']);
        $this->setPrice($product['price']);
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    public function getDiscount()
    {
        return $this->discount;
    }
}