<?php

namespace Task\Product;


interface IItem
{
    public function setName($name);

    public function setPrice($price);

    public function getName();

    public function getPrice();
}