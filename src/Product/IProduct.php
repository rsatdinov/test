<?php

namespace Task\Product;


interface IProduct
{
    public function setDiscount($discount);

    public function getDiscount();
}